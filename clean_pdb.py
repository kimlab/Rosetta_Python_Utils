
#goal: create a clean pdb file

import os
import log_maker_function as lmf

def clean_pdb( filePath = "yourpath", #path to pdb file
              keepLines = ["ATOM","TER"], #lines in pdb to keep
              suffix = "_clean",
             parent_dir = os.getcwd(), #parent directory of destination folder
             destPath = None, #name of destination folder
             overwrite = False, #overwrite existing file?
             new_directory = True, 
             logfile = True):
    
    pdbFileObj = open(filePath, 'r')
    
    
    if (type(destPath) == type("string")) == False:
        
        raise ValueError("for destPath (destination path), \'None\' is not acceptable")
        
    elif new_directory == True:
        
        if os.path.exists(parent_dir + "/" + destPath):
            
            raise ValueError("destination folder already exists")
            
        else:
            target_dir = os.path.join(parent_dir,destPath)
            os.mkdir(target_dir)
    else:
        target_dir = os.path.join(parent_dir,destPath)

    #now that target directory is set, give full filepath for new file accordingly
    
    filePath_split = filePath.split(sep = '/')
    oldFileName = filePath_split[len(filePath_split)-1]
    newFileName = target_dir + "/" + oldFileName[0:-4] + suffix + ".pdb"
    
    
    #check if file exists
    if os.path.exists(newFileName):
        
        if overwrite == True:
            os.remove(newFileName)
        else:
            raise ValueError("Overwrite == False, and file " + newFileName + " already exists")
    
    newFile = open(newFileName, 'a')
    
    for line in pdbFileObj:
        row = line.split()
        
        inclusion = False
        
        for i in range(0,len(keepLines),1):
            if row[0] == keepLines[i]:
                inclusion = True
        
        if inclusion == True:
            newFile.write(line)
            
    if logfile == True:
    
        default_dir = os.getcwd()
        os.chdir(parent_dir + "/" + destPath)
        
        # Write a log file or append to an existing logfile
        
        logFileName = oldFileName[0:-4] + suffix + "_log"
        
        logArgs = []
        logArgs.append("===================================")
        logArgs.append("cleaned pdb file")
        logArgs.append("output name: " + filePath[-8:-4] + suffix + ".pdb")
        logArgs.append("input pdb file: " + filePath) 
        logArgs.append("permitted lines: " + str(keepLines))
        logArgs.append("suffix: " + suffix)
        logArgs.append("parent directory: " + parent_dir)
        logArgs.append("destination path: " + destPath) 
        logArgs.append("overwrite: " + str(overwrite))
        logArgs.append("new directory: " + str(new_directory))
        
        lmf.make_logfile(logFileName, "clean_pdb", logArgs)
        
        os.chdir(default_dir)