#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jun  1 09:05:38 2017

@author: kimadmin
"""
import findFiles as FF
import os
import subprocess
import resFileUtilsV2 as RFU
import log_maker_function as lmf
import parseText as pt
import clean_pdb as CP
import time
import numpy
#ROSETTA INTERFACES ===========================================================

def relax_pdb(clean_pdb_path, 
              resfile_path, 
              rosetta_scripts_executable,
              rosetta_scripts_xml, 
              output_dir, 
              output_dir_parent):
    
    default_dir = os.getcwd()

    exist_dirs = FF.find_directory(output_dir, output_dir_parent, 
                              returnFirst = False)
    newDir = os.path.join(output_dir_parent, output_dir)
    
    if os.path.exists(newDir) == False:
        os.mkdir(newDir)
        
        
    #After making directory, construct the command
    os.chdir(newDir)
    command = (rosetta_scripts_executable + 
               " -in:file:s " + 
               clean_pdb_path +
               " -parser:protocol " + 
               rosetta_scripts_xml +
               " -parser:script_vars resFile=" + 
               resfile_path)
    command_split = command.split()
    
    #run the command
    subprocess.run(command_split)
    
#    #If logging, make log file
#    if logfile == True:
#        logArgs = []
#        logArgs.append("====================================")
#        logArgs.append("created relaxed PDB structure")
#        logArgs.append("input pdb file: " + clean_pdb_path)
#        lmf.make_logfile(logfile_name,"relax",logArgs)
    
    os.chdir(default_dir) #reset working directory 
    
    
    

#==============================================================================    
def score_pdb(clean_pdb_path, rosetta_scripts_executable, rosetta_scripts_xml,
              output_dir, output_dir_parent):
    
    default_dir = os.getcwd()
    
    exist_dirs = FF.find_directory(output_dir, output_dir_parent, 
                              returnFirst = False)
    
    newDir = output_dir_parent + "/" + output_dir 
   
    if len(exist_dirs) == 0:
        os.mkdir(output_dir_parent + "/" + output_dir)
        
        
    #After making directory, construct the command
    
    os.chdir(newDir) #changing directory. Rosetta likes to put output in wd
    
    command = (rosetta_scripts_executable + 
               " -in:file:s " + 
               clean_pdb_path +
                " -parser:protocol " + 
                rosetta_scripts_xml +
                " -out:prefix scoring_")
    command_split = command.split()
    
    subprocess.run(command_split)
    
    os.chdir(default_dir) #reset working directory
    
#==============================================================================

def mutate_and_pack(relaxed_pdb_path, 
                    rosetta_scripts_executable,
                    rosetta_scripts_xml,
                    output_dir, 
                    output_dir_parent,
                    resfile_path, 
                    ncaa_list_path, 
                    residue_list,
                    mut_res_commands = ['EX 1', 'EX 2'], 
                    suffix = "",
                    logfile = True): #logging will occur in a predetermined fashion
                                    # logfile goes to output directory, logfile name
                                    #determined by name of output directory
                                    
                                    #note: since putting output into existing directories
                                    # is forbidden, no need to check that a logfile
                                    # already exists
    
    #GOAL: GIVEN A LIST OF RESIDUES AND A LIST OF NON CANONICAL AMINO ACIDS
    #MAKE MUTATED STRUCTURES AT ALL SPECIFIED POSITIONS INCORPORATING EACH
    #ONE OF THE NON CANONICAL AMINO ACIDS. THIS WILL MAKE SINGLE EDITS TO
    #RESFILES
    
    #Give logfile a name. Doesn't mean we're making it necessarily    
    logfile_name = output_dir + "_process_logfile.txt"
    
    
    
    default_dir = os.getcwd() #store current wd in a variable
            
    
    newDir = os.path.join(output_dir_parent, output_dir) 
   
    if os.path.exists(newDir) == False:
        os.mkdir(newDir)
    else:
        raise ValueError("Directory already exists")
        
    #Switch to new directory
    
    os.chdir(newDir) #changing directory. Rosetta likes to put output in wd

    #If we're writing logfile, write some lines detailing inputs
    if logfile == True:
        
        
        logArgs = []
        logArgs.append("=============================================")
        logArgs.append("BEGIN mutate_and_pack function")
        logArgs.append("pdb file: " + relaxed_pdb_path)
        logArgs.append("rosetta_scripts executable: " + rosetta_scripts_executable)
        logArgs.append("output directory full path: " + os.path.join(output_dir_parent, output_dir))
        logArgs.append("resfile filepath: " + resfile_path)
        logArgs.append("ncaa list filepath: " + ncaa_list_path)
        logArgs.append("mutant residue commands: " + str(mut_res_commands))
        logArgs.append("residues selected for mutation: " + str (residue_list))
        lmf.make_logfile(logfile_name, "mutate_and_pack", logArgs)
        
    
    #Load NCAA List into a list object
    ncaa_file_obj = open(ncaa_list_path,'r')
    ncaa_list = RFU.parseText2(ncaa_file_obj)
    
    #Load resfile array as a template for editing (we'll make lots of 
    # temporary resfiles)
    resFileArray = RFU.resfile_to_resFileArray(path = resfile_path,
                                               logfile = logfile,
                                               logfile_dir = newDir,
                                               logfile_name = logfile_name,
                                               logfile_append = True,
                                               logIndicator = str(0))
    
    original_suffix = suffix #save original suffix
    
    for i in range(0,len(residue_list),1):
        
        #expecting an i x 2 list with each row i containing entries
        #for residue number and chain
        resNum = residue_list[i][0]
        chain = residue_list[i][1]
        res_chain_dir = newDir + "/chain_" + chain + "_res_" + str(resNum)
        os.mkdir(res_chain_dir) #This line and line above added to
        #make folder for each residue at each chain where changes occured
        #Stores output pdbs and scorefiles
        
        os.chdir(res_chain_dir) #change working directory to route output
                                #to above created folder
        
        for n in range(0,len(ncaa_list),1):
            
            if logfile == True: #If making logfile, log this info
                os.chdir(newDir)
                logArgs1 = []
                logArgs1.append("======================================")
                logArgs1.append("ITERATION " + str(n + 1))
                logArgs1.append("creating edited set of commands")
                lmf.make_logfile(logfile_name, "edit_commands_" + str(n+1), 
                                 logArgs1)
                del logArgs1
                os.chdir(res_chain_dir)
            
            #for each ncaa, specify ncaa, create a modified resfile,
            #and input said resfile into rosetta scripts
            
            #select ncaa from list
            ncaa = ncaa_list[n][0]
            
            #construct commands for single residue editing, based on chain
            #residue number, and ncaa selected
            edit_commands = ['EMPTY', 'NC', ncaa] + mut_res_commands
            
            #create an edited array of resfile info, identical to
            #that contained in original resfile but with the specified
            #residue mutated. Include additional commands (mut_res_commands)
            single_res_edited = RFU.changeCommSingle(resFileArray = resFileArray, 
                                           chain = [chain], 
                                           position = resNum,
                                           Commands = edit_commands,
                                           logfile = logfile,
                                           logfile_dir = newDir,
                                           logfile_name = logfile_name,
                                           logfile_append = True,
                                           logIndicator = str(n+1))
            #create the new resfile (a temporary file)
            
            if logfile == True:
                os.chdir(newDir)
                logArgs2 = []
                logArgs2.append("=======================================")
                logArgs2.append("creating temporary resfile")
                lmf.make_logfile(logfile_name, "make_temp_resfile_" + str(n+1),
                                 logArgs2)
                del logArgs2
                os.chdir(res_chain_dir)
                
            resFileName = "temp.resfile"
            RFU.array2ResFile(single_res_edited, 
                              res_chain_dir, 
                              resFileName,
                              logfile = logfile,
                              logfile_dir = newDir,
                              logfile_name = logfile_name,
                              logfile_append = True,
                              logIndicator = str(n+1))
            
            #above line edited 6.5.17 
            suffix = (suffix + "_residue_" + str(resNum) + "_chain_" +
                      chain + "_sub_" + ncaa)
            
            if logfile == True:
                
                os.chdir(newDir)
                logArgs3 = []
                logArgs3.append("=======================================")
                logArgs3.append("executing command in rosetta scripts")
                lmf.make_logfile(logfile_name, "mutate_and_pack_exec_command" + str(n+1), logArgs3)
                del logArgs3
                os.chdir(res_chain_dir)
                
            
            #Construct the command for rosetta
            rosetta_command = (rosetta_scripts_executable + 
                               " -in:file:s " +
                               relaxed_pdb_path + 
                               " -parser:protocol " +
                               rosetta_scripts_xml +  
                               " -parser:script_vars resFile=" +
                               "temp.resfile" + 
                               " -out:suffix " +
                               suffix)
            
            rosetta_command_split = rosetta_command.split()
            
            #Run the command in rosetta_scripts
            subprocess.run(rosetta_command_split)
            
            #remove resfile to avoid taking up space on HD or other storage
            
            if logfile == True:
                os.chdir(newDir)
                logArgs4 = []
                logArgs4.append("=======================================")
                logArgs4.append("removing temporary resfile")
                lmf.make_logfile(logfile_name, "remove_temp_resfile_" + str(n+1), logArgs4)
                del logArgs4
                os.chdir(res_chain_dir)
            
            os.remove(os.path.join(res_chain_dir,"temp.resfile"))
            
            suffix = original_suffix
            
            
            
        
    os.chdir(default_dir) #change working directory in python back to default
    
    if logfile == True:
        
        os.chdir(os.path.join(output_dir_parent,output_dir))
        
        logArgs5 = []
        logArgs5.append("=========================================")
        logArgs5.append("Finished program")
        lmf.make_logfile(logfile_name, "finished", logArgs5)
    
        os.chdir(default_dir)
        
#==============================================================================

def make_scoretable(output_dir, overwrite = False):
    
    #ouput file should be a file of the format:
        
        #SCORE: <total rosetta_score> <filename>
    
    
    dir_split = output_dir.split('/')
    
    #give a filename that is the folder name with score_table.txt appended
    fileName = dir_split[len(dir_split)-1] + "_score_table.txt"
    fullPath = os.path.join(output_dir,fileName) #full filepath
    
    #do the usual check to see if file exists. if it does, either overwrite or raise error
    if (os.path.exists(fullPath)):
        
        if overwrite == False:
        
            raise ValueError('specified to not overwrite file (overwrite = False), ' +
                             fileName + ' already exists in specified output directory')
        else:
            
            os.remove(os.path.join(output_dir,fileName))
            
    #Make the list to contain the scorefile info
    
    scoretable = [] #just an empty list, to have entries appended
    
    #scan through the output directory for all folders. folders should all have
    #ouput from the mutate_and_pack function
    with os.scandir(output_dir) as it:
    
        for entry in it:
        
            if entry.is_dir():
            
                folder = entry.name
                full_path_folder = os.path.join(output_dir,folder)
                #scan through all entries of given folder, and 
                with os.scandir(full_path_folder) as that:
                    
                    for sub_entry in that:
                        
                        #if the entry within folder is a file and it has score
                        #and .sc in the name, proceed to read file's contents
                        if ((sub_entry.is_file()) & ('score' and '.sc' in sub_entry.name)):
                            
                            scorefile_fullname = os.path.join(full_path_folder,sub_entry.name)
                            
                            scorefile_obj = open(scorefile_fullname, 'r') #make file object for scorefile
                            scorefile_parsed = pt.parseText2(scorefile_obj) #multi dimensional list
                            indexLast = len(scorefile_parsed) - 1
                            scoretable.append(scorefile_parsed[indexLast][0:2])
                            scoretable[len(scoretable)-1][1] = float(scoretable[len(scoretable)-1][1])
                            #second element of row should be  a number as a string
                            #coerce to float
                            scoretable[len(scoretable)-1].append(sub_entry.name)
                            #append filename to the row    
    #By this point you've constructed the scoretable list. Now we need to order it
    
    scoretable.sort(key = lambda row:row[1])
    header = [['rosetta_total_score', ' ', 'filename']]
    scoretable = header + scoretable
    
    #Now that you've sorted, make and write the file
    newFileObj = open(fullPath, 'a')
    
    writeToFile = "" #dynamically constructed string
    
    for i in range(0,len(scoretable),1):
        
        newString= "" # start new in each iteration i, get constructed from the n
                        #elements in scoretable[i]
        scoretable[i][1] = str(scoretable[i][1]) #reconvert the float
                                                # total rosetta score to a string
        for n in range(0,len(scoretable[i])):
            
            if n == 0:
            
                newString = newString + scoretable[i][n]
                
            elif n < (len(scoretable[i])-1):
                newString = newString + " " + scoretable[i][n]
            
            else:
                newString = newString + " " + scoretable[i][n] + "\n"
                #if at the end of entries for scoretable[i][n], cut the line off
            
        #with newstrign constructed, add to write file
        
        writeToFile = writeToFile + newString
        
        del newString
        
    
    newFileObj.write(writeToFile) #and you've written to the file!
#==============================================================================    
def make_scoretable_ensemble(output_dir, pdb_name, overwrite = False):
    
    #ouput file should be a file of the format:
        
        #SCORE: <total rosetta_score> <filename>
    
        data_array = [[],[],[],[],[],[],[]]
        #2D array with the following
                        #Column 1: group
                        #Column 2: chain
                        #Column 3: residue num
                        #Column 4: substitution (NCAA substitution)
                        #Column 5: total score (total rosetta score for mutant)
                        #Column 6: total score of relaxed pdb (total rosetta score)
                        #Column 7: total score - total score of relaxed pdb
        found_a_group = False
        with os.scandir(output_dir) as it1:
            
            for entry in it1:
                
                if entry.is_dir() & (pdb_name in entry.name):
                    
                    found_a_group = True
                    group_name = entry.name
                    group_path = os.path.join(output_dir,group_name)
                    
                    with os.scandir(group_path) as it2:
                        
                        #when I refer to group, I presume the group will be one
                        #of several sets of mutant pdbs derived from a single
                        #pdb that was derived from an md simulation
                        #once you're in a group directory \, find relaxed file
                        #find its score, and store that score
                        
                        #find 
                        found_relaxed = False
                        
                        counter = 0 
                        #This corresponds to 1 group directory, counting number of times a scorefile
                        #is picked up. Purpose is to add appropriate number of 
                        #relaxed pdb rosetta score copies to the data_array
                        #The columns of mutant structure score and relaxed pdb score
                        #will be converted to numpy arrays, and then the difference
                        #in score calculated (as a numpy array), will be calculated
                        
                        for entry in it2:
                            #within group directory, find relaxed folder and mutant structures directory
                            
                            if entry.is_dir() & (entry.name == 'relaxed_pdb'):
                                
                                relaxed_folder = entry.name
                                relaxed_dir = os.path.join(group_path, relaxed_folder)
                                rlx_dir_list = os.listdir(relaxed_dir)
                                
                                for i in range(0,len(rlx_dir_list),1):
                                    
                                    if '.sc' in rlx_dir_list[i]:
                                        
                                        #once in relaxed directory, if you find the scorefile, retrieve score
                                        
                                        rlx_scorefile = rlx_dir_list[i]
                                        rlx_fileObj = open(os.path.join(relaxed_dir, rlx_scorefile), 'r')
                                        rlx_parsed = pt.parseText2(rlx_fileObj)
                                        rlx_score = float(rlx_parsed[2][1])
                                        
                                        found_relaxed = True 
                                        
                                if found_relaxed == False:
                                    
                                    print('no relaxed pdb scorefile found in ' + group_path)
                                    break
                            
                            elif entry.is_dir() & (entry.name == 'mutant_structures'):
                                
                                #move into mutant_structures folder
                                
                                mut_str_folder = entry.name #mut structure parent folder
                                mut_dir_path = os.path.join(group_path, mut_str_folder)
                                
                                with os.scandir(mut_dir_path) as it3:
                                    
                                    for entry in it3:
                                        
                                        if entry.is_dir():
                                            
                                            chain_res_folder = entry.name
                                            chain_res_dir_path = os.path.join(mut_dir_path,chain_res_folder)
                                            chain_res_files = os.listdir(chain_res_dir_path)
                                            
                                            for n in range(0,len(chain_res_files),1):
                                                #we are now in the folder containing mutant pdbs and their scores
                                                if '.sc' in chain_res_files[n]:
                                                    
                                                    scorefile_name = chain_res_files[n]
                                                    scorefile_path = os.path.join(chain_res_dir_path, scorefile_name)
                                                    scorefile_obj = open(scorefile_path, 'r')
                                                    scorefile_parsed = pt.parseText2(scorefile_obj)
                                                    mut_score = scorefile_parsed[2][1] #total rosetta score for individual pdb
                                                    
                                                    #retrieve chain, residue and substitution info
                                                    scorefile_name_split = scorefile_name.split('_')
                                                    chain = scorefile_name_split[4]
                                                    residue = scorefile_name_split[2]
                                                    sub = scorefile_name_split[-1][0:-3] #.sc will be attached to end, need to remove
                                                    
                                                    #Add chain, residue, substitution, and score to one 'row' of data_array
                                                    data_array[0].append(group_name)
                                                    data_array[1].append(chain)
                                                    data_array[2].append(residue)
                                                    data_array[3].append(sub)
                                                    data_array[4].append(float(mut_score))
                                                    counter = counter + 1
                                                    
                            
                        data_array[5] = data_array[5] + [rlx_score]*counter
        
        #search of directories has ended. Now calculate differences in rosetta score
        
        if found_a_group == False:
            raise ValueError('did not find any folders under output_dir ' + output_dir +
                             ' with ' + pdb_name + ' in the folder name. Check that pdb name was entered correctly')
        
        mutant_score_vect = numpy.array(data_array[4])
        relaxed_score_vect = numpy.array(data_array[5])
        neg_rlx_score_vect = numpy.multiply(-1,relaxed_score_vect)
        score_diff = numpy.add(mutant_score_vect,neg_rlx_score_vect)
        
        #and assign to 7th column of data array
        data_array[6][:] = score_diff[:]        
        
        #make a file and write all the data to it
        
        score_table_name = pdb_name[0:-3] + '_ensemble_scoretable.txt'
        
        score_table_path = os.path.join(output_dir,score_table_name)
        
        if (os.path.exists(score_table_path)) & (overwrite != True):
            raise ValueError(score_table_path + ' exists and overwrite != True')
        elif overwrite == True:
            os.remove(score_table_path)
        
        
        len0 = len(data_array[0])
        len1 = len(data_array[1])
        len2 = len(data_array[2])
        len3 = len(data_array[3])
        len4 = len(data_array[4])
        len5 = len(data_array[5])
        len6 = len(data_array[6])
        
        #check if all lengths in data array columns the same
        
        len_pass = (len0 == len1 == len2 == len3 == len4 == len5 == len6)
        
        if len_pass == False:
            ValueError('lengths of all elements [n] in data_array must be equal')
        
        print('writing score_table')
        score_table_fileObj = open(score_table_path, 'a')
        first_line = 'group\tchain\tres\tsub\tmut_scr\trlx_scr\tdiff\n'
        score_table_fileObj.write(first_line)
        newLines = ''
        
        for i in range(0,len0,1):
            #Construct the newline string for each row, and write it
            
            
                
            for n in range(0,7,1):
                
                if n < 6:
                    add_on = str(data_array[n][i]) + '\t'
                    
                elif n == 6:
                    add_on = str(data_array[n][i]) + '\n'
                
                newLines = newLines + add_on
                
        score_table_fileObj.write(newLines)
        
                            
#==============================================================================

def mut_pack_pdb_seq(pdbList, #Folder containing all pdbs
                     pdbFolder,
                     rosetta_scripts_executable, #Filepath of the rosetta scripts executable
                     rosetta_scripts_xml_relax, #XML you'd like to use for relax protocol
                     rosetta_scripts_xml_pack, #XML you'd like to use for the mutate and pack protocol
                     parent_parent_dir, #parent dir for all output directories
                     ncaa_list_path,
                     residue_list_path,
                     relax_commands = ['EX 1', 'EX 2'],
                     pack_commands = ['EX 1', 'EX 2'],
                     mut_commands = ['EX 1', 'EX 2']
                     ):
    
    #General workflow: from a list of pdbs, from folder, go through each pdb name
    # for each pdb, make an output folder that is a subdirectory of the 'parent
    #parent dir' (parent dir for all pdb output folders) and go through the process
    # of making a clean pdb from the raw pdb, making a raw resfile from the clean pdb,
    # making two edited resfiles depending on desired commands for relax and pack
    # then run the relax protocol with the relax resfile and clean pdb as input
    # Afterwards, take the resulting relaxed pdb, the list of residues you want to mutate
    # and the list of NCAAs you want to use, and the pack resfile as inputs 
    # to the mutate and pack function.
    
    completion_times = []
    
    for i in range(0,len(pdbList),1):
        
        start_time = time.time()
        
        pdbName = pdbList[i] #name for pdb
        
        output_folder_name = pdbName[0:-4] + '_output'
        output_dir = os.path.join(parent_parent_dir, output_folder_name)
        clean_pdb_dir = os.path.join(output_dir,'cleaned_pdbs') #directory for clen pdbs
        raw_resfile_dir = os.path.join(output_dir,'raw_resfile')
        mod_resfile_dir = os.path.join(output_dir,'mod_resfile')
        relaxed_pdb_dir = os.path.join(output_dir,'relaxed_pdb')
        
        
        os.mkdir(output_dir)
        os.mkdir(clean_pdb_dir)
        os.mkdir(raw_resfile_dir)
        os.mkdir(mod_resfile_dir)
        os.mkdir(relaxed_pdb_dir)
        
        
        raw_pdb_path = os.path.join(pdbFolder,pdbName) #complete path to pdb
        
        CP.clean_pdb(filePath = raw_pdb_path, #path to pdb file
                      keepLines = ["ATOM","TER"], #lines in pdb to keep
                      suffix = "_clean",
                      parent_dir = output_dir, #parent directory of destination folder
                      destPath = "cleaned_pdbs", #name of destination folder
                      overwrite = False, #overwrite existing file?
                      new_directory = False, 
                      logfile = True)
        
        clean_pdb_name = pdbName[0:-4] + '_clean.pdb'
        clean_pdb_path = os.path.join(clean_pdb_dir,clean_pdb_name)
        raw_resfile_name = pdbName[0:-4] + '_raw_resfile.resfile'
        
        RFU.make_resfile(pdbPath = clean_pdb_path, #filepath to pdb file you wish to generate resfile for
                         resfile_name = raw_resfile_name, #name for resfile
                         destPath = 'raw_resfile', #enter a string here for the destination folder (not a full directory path)
                         parent_directory = output_dir, #parent directory of destination
                         new_directory = False, #boolean. Do we make a new directory?
                         overwrite = False, #boolean. do we overwrite an existing resfile?
                         Use_Input_SC = True, #boolean. If true, will write USER_INPUT_SC to first line
                                         #meaning that rosetta will allow experimentally determined rotamers
                                         #in the output structure it generates (if using packer)
                         allowables = ["ATOM","TER"], #which lines do we allow? format as a list with string elements
                         logfile = True,
                         logIndicator = "")
        
        raw_resfile_path = os.path.join(raw_resfile_dir,raw_resfile_name)
        relax_mod_resfile_name = pdbName[0:-4] + '_relax_mod.resfile'
        relax_mod_resfile_log_name = relax_mod_resfile_name[0:-8] + '_log.txt'
        relax_mod_resfile_path = os.path.join(mod_resfile_dir,relax_mod_resfile_name)
        
        RFU.edit_resfile(source_resfile = raw_resfile_path, #path to resfile, full or relative
                         output_dir = mod_resfile_dir, #Note: This must be a full directory path, not the name of a subdirectory
                         new_directory = False, 
                         fName = relax_mod_resfile_name, 
                         overwrite_resfile = False,
                         edit_mode = 'chains', 
                         chains = ['ALL'], 
                         position = None,
                         AAstart = None, #if specified range, AA start specifies first residue of range, AAend specifies last
                         AAend = None, 
                         Commands = relax_commands,
                         logfile = True, 
                         logfile_name = relax_mod_resfile_log_name, #Note that the logfile is constructed by appending logfile info
                                             #from multiple operations (resfile to array, change commands, array to resfile)
                                             #Thus logfile_append true for all functions called within this function
                                             #that make a logfile. 
                         logfile_append = False, #If logfile append = false, make another logfile for the entire process
                         logIndicator = "")
        
        pack_mod_resfile_name = pdbName[0:-4] + '_pack_mod.resfile'
        pack_mod_resfile_log_name = pack_mod_resfile_name[0:-8] + '_log.txt'
        pack_mod_resfile_path = os.path.join(mod_resfile_dir, pack_mod_resfile_name)
        
        RFU.edit_resfile(source_resfile = raw_resfile_path, #path to resfile, full or relative
                         output_dir = mod_resfile_dir, #Note: This must be a full directory path, not the name of a subdirectory
                         new_directory = False, 
                         fName = pack_mod_resfile_name, 
                         overwrite_resfile = False,
                         edit_mode = 'chains', 
                         chains = ['ALL'], 
                         position = None,
                         AAstart = None, #if specified range, AA start specifies first residue of range, AAend specifies last
                         AAend = None, 
                         Commands = pack_commands,
                         logfile = True, 
                         logfile_name = pack_mod_resfile_log_name, #Note that the logfile is constructed by appending logfile info
                                             #from multiple operations (resfile to array, change commands, array to resfile)
                                             #Thus logfile_append true for all functions called within this function
                                             #that make a logfile. 
                         logfile_append = False, #If logfile append = false, make another logfile for the entire process
                         logIndicator = "")
        
        
        
        relax_pdb(clean_pdb_path = clean_pdb_path, 
                  resfile_path = relax_mod_resfile_path, 
                  rosetta_scripts_executable = rosetta_scripts_executable,
                  rosetta_scripts_xml = rosetta_scripts_xml_relax, 
                  output_dir = 'relaxed_pdb', #refers to the directory where relaxed pdb is put
                  output_dir_parent = output_dir) #output_dir in this instance refers to the pdb specific parent directory
                                                  #which contains all outputs for a given pdb. it is the parent of relaxed_pdb_dir
                                                  
        relaxed_pdb_name = clean_pdb_name[0:-4] + '_0001.pdb'
        relaxed_pdb_path = os.path.join(relaxed_pdb_dir, relaxed_pdb_name)
        

        residue_list_fileObj = open(residue_list_path, 'r')

        residue_list = pt.parseText2(residue_list_fileObj)

        #below: an ugly way to make sure everything in column 1 of the residue list is of type integer
        for i in range(0,len(residue_list),1):
            residue_list [i][0] = int(residue_list[i][0])
        
        mutate_and_pack(    relaxed_pdb_path = relaxed_pdb_path, 
                            rosetta_scripts_executable = rosetta_scripts_executable,
                            rosetta_scripts_xml = rosetta_scripts_xml_pack,
                            output_dir = 'mutant_structures', 
                            output_dir_parent = output_dir,
                            resfile_path = pack_mod_resfile_path, 
                            ncaa_list_path = ncaa_list_path, 
                            residue_list = residue_list,
                            mut_res_commands = mut_commands, 
                            suffix = "",
                            logfile = True)        
                
        end_time = time.time()
        completion_times.append(end_time-start_time)
        
    print(completion_times)
                
    