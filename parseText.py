import re
def parseTextFunc(String, delimiters):

	myArray = [] #To be a 2D array which holds the 'words'
	word="" #container for string
	myRow=[]
	cont=True #continue with word construction
	m=0 # a back index that will be used to index the first letter of a word given position i in String
	lastCharIsChar = False
	lenString= len(String)
	for i in range( 0 , lenString , 1 ):
			cont = True
			for n in range(0,len(delimiters),1):
				if String[i] == delimiters[n]:
					cont = False
				#if indexed portion of string == any of the
				#delimiters, cont = false, directing to
				#completion of word and making a new line
				#if necessary
		
			if cont == False: #If you've hit a delimiter
				if lastCharIsChar == True: #If the last character was not a delimiter
					lastCharIsChar = False #In next run, the last character will have been a delimiter
					word=String[(i-m):i]
					m = 0 #reset back index to get components of string		
					if String[i] == "\n":
						myRow.append(word) #add word to row
						myArray.append(myRow) #add row to array
						myRow = []
						word = ""
					#cut the line, start from anew line in M
					
					else:
						myRow.append(word)
						word = ""
			elif i == lenString-1: #else if you've hit the end of the string
				#follow the general procedure to construct a word, append to row
				#append row to array
				
				word = String[(i-m):i+1]
				myRow.append(word)
				myArray.append(myRow)
			else:
				m = m+1 #increase back index by 1
				lastCharIsChar = True #in next iteration, last character will not have been
							#a delimiter
	return(myArray) #output array

def parseText2(fileObj):
    myArray = []
    for line in fileObj:
        row = line
        myArray.append(row.split())
    return(myArray)

def parseText_RE(fileObj,regex):
    myArray = []
    for line in fileObj:
        row = line
        myArray.append(re.split(regex,row))
    return(myArray)    

