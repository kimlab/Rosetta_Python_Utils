#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul  5 09:29:41 2017

@author: owenwhitley
"""

import pytraj as pt
import os

def mdcrd_to_pdb(mdcrd_file,top,output_dir):
    
    traj = pt.load(mdcrd_file,top = top, mask = '!:WAT,CL-,Na+')
    last_frame = traj[[-1]]
    
    
    mdcrd_filename = mdcrd_file.split('/')[-1]
    pdb_filename = mdcrd_filename[0:-6] + '.pdb'
    pdb_filepath = os.path.join(output_dir,pdb_filename)
    pt.write_traj(pdb_filepath,last_frame, overwrite = True)
    
def redo_chain_num(input_pdb, output_pdb, chainfile, overwrite = False):
    chainfile_obj = open(chainfile,'r')
    chain_list = []
    
    if (os.path.exists(output_pdb)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(output_pdb):
        os.remove(output_pdb)
    
    for line in chainfile_obj:
        chain_list.append(line.split())
    
    inp_fileobj = open(input_pdb, 'r')
    out_fileobj = open(output_pdb, 'a')
    
    N = 0 #Index added to start as we give residue numbers
    chain_ind = 0 #Index for chain info in chain_list
    #Setup chain ID, chain start and end
    
    chain_start = int(chain_list[chain_ind][1])
    chain_end = int(chain_list[chain_ind][2])
    chain_ID = str(chain_list[chain_ind][0])
    first_time = True
    
    for line in inp_fileobj:
        
        if line[0:4] == 'ATOM':
            
            
            
            if first_time == True:
                first_time = False
                lastTer = False
                
            else:
                
                if (last_inp_resnum < int(line[22:26])) & (lastTer == False):
                    N = N + 1
                elif lastTer == True:
                    lastTer = False
            res_num = chain_start + N
            res_num_string = '    '
            res_num_string = res_num_string[0:(len(res_num_string)-len(str(res_num)))] + str(res_num)
            newLine = line[0:21] + chain_ID + res_num_string + line[26:len(line)]
            last_inp_resnum = int(line[22:26])
            
            serial = int(line[6:11])
            res_name = line[17:21]    
                    
                
            if N > (chain_end-chain_start + 1):
                raise ValueError('At residue ' + str(res_num) + ' chain ' + chain_ID +
                                 ' residue number exceeds stated end residue number ' + 
                                 str(chain_end))
            
        elif line[0:3] == 'TER':
            
            if  N != (chain_end-chain_start):
                raise ValueError('TER line should only come after the stated' +
                                 ' end of the chain, for chain ' + chain_ID)
            N = 0
#            newLine = line[0:22] + res_num_string + line[len(line)-1]
            #Constructing a TER line
            record_name = 'TER   '
            serial = serial + 1
            empty = ' ' #empty string
            serial_str = empty*5
            serial_str = serial_str[0:(len(serial_str)-len(str(serial)))] + str(serial)
            filler = empty*6 #fill columns 12:17
            #res_name already specified
            #chainID already specified
            #residue sequence specifier (res_num_string) already specified
            
            newLine = (record_name + serial_str + filler + res_name + chain_ID + res_num_string + ' \n')
            
            if chain_ind != (len(chain_list)-1):
                chain_ind = chain_ind + 1
                chain_start = int(chain_list[chain_ind][1])
                chain_end = int(chain_list[chain_ind][2])
                chain_ID = str(chain_list[chain_ind][0])
                #Change chain index and chain related variables for new chain
                lastTer = True #If this is true, for the next atom line, N will not be incremented
                #lastTer is reverted to false after going through the next atom line
                
                serial = int(line[6:11])
                res_name = line[17:21]
                
        elif 'ENDMDL' in line:
            #Constructing a TER line
            record_name = 'TER   '
            serial = serial + 1
            empty = ' ' #empty string
            serial_str = empty*5
            serial_str = serial_str[0:(len(serial_str)-len(str(serial)))] + str(serial)
            filler = empty*6 #fill columns 12:17
            #res_name already specified
            #chainID already specified
            #residue sequence specifier (res_num_string) already specified
            
            newLine = (record_name + serial_str + filler + res_name + chain_ID + res_num_string + ' \n')
            
            
        else:
            
            newLine = line
            
        out_fileobj.write(newLine)
        
def change_temp_factor(in_pdb_file,out_pdb_file, overwrite = False):
    
    if (os.path.exists(out_pdb_file)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        
    elif os.path.exists(out_pdb_file):
        os.remove(out_pdb_file)
    
    inp_file_obj = open(in_pdb_file, 'r')        
    output_file_obj = open(out_pdb_file, 'a')
    
    for line in inp_file_obj:
        
        if line.split()[0] == 'ATOM':
            
            if line[62:66] == '0.00':
                
                newLine = line[0:62] + '0.50' + line[66:len(line)]
            
            else:
                
                newLine = line
                
        else:
            newLine = line
            
        output_file_obj.write(newLine)
        
def pdb_rename_HIS(in_pdb_file,out_pdb_file, overwrite = False, verbose = True):
    
    if (os.path.exists(out_pdb_file)) & (overwrite == False):
        raise ValueError('file exists with same name as output file, overwirte specified as False')
        print(os.path.exists(out_pdb_file))
        print(overwrite)
        
    elif os.path.exists(out_pdb_file):
        os.remove(out_pdb_file)
    
    inp_file_obj = open(in_pdb_file, 'r')    
    targets = ['HIP','HID','HIE']
    
    output_file = open(out_pdb_file, 'a')
    edited_residues = []
    
    for line in inp_file_obj:
        
        cont = False
        
        for i in range(0, len(targets), 1):
            
            if targets[i] in line:
                cont = True
                
        if cont == True:
            newLine = line[0:17] + 'HIS' + line[20:len(line)]
            resnum = line[23:29]
            res_chain = str(resnum) + line[21]
            
            if (res_chain in edited_residues) == False:
                edited_residues.append(res_chain)
        else:
            newLine = line
            
        output_file.write(newLine)
    
    if verbose == True:    
        if len(edited_residues) > 0:
            
            print('The following residues were recognized as histidine and\
     edited to HIS:')
            
            print(edited_residues[:])
            
        else:
            print('No residues recognized as being histidine with amber naming')